import './App.css';
import { useState } from "react";

function randomRGB() {
  const randomNumber = (min, max) =>
      min + Math.floor(Math.random() * (max - min + 1));

  const r = randomNumber(0, 255).toString(16);
  const g = randomNumber(0, 255).toString(16);
  const b = randomNumber(0, 255).toString(16);

  return `#${r}${g}${b}`;
}

function Block(props) {
  return (
    <div
      className="block"
      style={{ backgroundColor: props.color }}
      onClick={props.onClick}
    >{props.children}</div>
  )
}

function App() {
  const NUMBER_OF_BLOCKS = 9;
  const [colors, setColors] = useState(
    new Array(NUMBER_OF_BLOCKS).fill(null).map(() => randomRGB())
  );

  const changeColor = () => setColors(
    new Array(NUMBER_OF_BLOCKS).fill(null).map(() => randomRGB())
  );

  return (
    <div className="App">
      <section className="section section-a">
        <Block color={colors[0]} onClick={changeColor}>1</Block>
        <div className="right">
          <Block color={colors[1]} onClick={changeColor}>2</Block>
          <Block color={colors[2]} onClick={changeColor}>3</Block>
          <Block color={colors[3]} onClick={changeColor}>4</Block>
        </div>
      </section>

      <section className="section section-b">
        <div className="left">
          <Block color={colors[4]} onClick={changeColor}>5</Block>
          <Block color={colors[5]} onClick={changeColor}>6</Block>
        </div>
        <Block color={colors[6]} onClick={changeColor}>7</Block>
        <div className="right">
          <Block color={colors[7]} onClick={changeColor}>8</Block>
          <Block color={colors[8]} onClick={changeColor}>9</Block>
        </div>
      </section>
    </div>
  );
}

export default App;
