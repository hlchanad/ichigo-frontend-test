# Ichigo Frontend Test

---

## Requirements

1. [Docker](https://docs.docker.com/install/)

2. [Docker Compose](https://docs.docker.com/compose/install/)


## How to run it?

1. Clone the repository:
```shell
$ git clone git@bitbucket.org:hlchanad/ichigo-frontend-test.git
```

2. Build the application
```shell
$ docker-compose build
```

3. Run the server
```shell
$ docker-compose up web
```
