FROM node:16.14.0-alpine3.15 as builder

WORKDIR /app

COPY ./package* ./

RUN npm ci

COPY . .

RUN npm run build

FROM node:16.14.0-alpine3.15

WORKDIR /app

COPY ./package* ./

RUN npm ci --production

COPY --from=builder /app/build /app/build

RUN npm i -g serve

CMD ["serve", "-l", "80", "./build"]
